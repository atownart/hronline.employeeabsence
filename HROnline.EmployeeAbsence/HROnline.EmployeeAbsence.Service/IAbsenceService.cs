﻿using System.Collections.Generic;
using HROnline.EmployeeAbsence.Entities;

namespace HROnline.EmployeeAbsence.Service
{
    public interface IAbsenceService
    {
        void Add(Absence absence);

        IEnumerable<Absence> GetAll();
    }
}
