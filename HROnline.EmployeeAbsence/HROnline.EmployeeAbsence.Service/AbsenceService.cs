﻿using System;
using System.Collections.Generic;
using HROnline.EmployeeAbsence.Entities;
using HROnline.EmployeeAbsence.Providers;

namespace HROnline.EmployeeAbsence.Service
{
    public class AbsenceService:IAbsenceService
    {
        private readonly IAbsenceRepostory _absenceRepository;

        public AbsenceService(IAbsenceRepostory absenceRepository)
        {
            _absenceRepository = absenceRepository;
        }

        public void Add(Absence absence)
        {
            if (absence == null)
            {
                throw new ArgumentNullException();
            }

            _absenceRepository.Add(absence);
        }

        public IEnumerable<Absence> GetAll()
        {
            return _absenceRepository.GetAll();
        }
    }
}
