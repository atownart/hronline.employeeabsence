﻿using System;
using System.Collections.Generic;
using HROnline.EmployeeAbsence.Entities;
using HROnline.EmployeeAbsence.Web.ViewModels;

namespace HROnline.EmployeeAbsence.UnitTests
{
    public static class AbsenceTestHelper
    {
        public static IEnumerable<Absence> DummyAbsences(int amount)
        {
            var entities = new List<Absence>();
            for (var i = 0; i < amount; i++)
            {
                entities.Add(new Absence() { StartDateTime = DateTime.Now.AddDays(-3), EndDateTime = DateTime.Now});   
            }
            return entities;
        }

        public static IEnumerable<AbsenceViewModel> DummyAbsencesViewModel(int amount)
        {
            var entities = new List<AbsenceViewModel>();
            for (var i = 0; i < amount; i++)
            {
                entities.Add(new AbsenceViewModel(){ StartDateTime = DateTime.Now.AddDays(-3), EndDateTime = DateTime.Now });
            }
            return entities;
        }
    }
}
