﻿using System;
using HROnline.EmployeeAbsence.Entities;
using HROnline.EmployeeAbsence.Web.Mappers;
using HROnline.EmployeeAbsence.Web.ViewModels;
using NUnit.Framework;

namespace HROnline.EmployeeAbsence.UnitTests.Web.Mappers
{
    [TestFixture]
    public class AbsenceMapperTests
    {
        [Test]
        public void Map_WithAbsenceViewModel_ReturnsAbsenceEntity()
        {
            var viewModel = new AbsenceViewModel();
            var absenceMapper = new AbsenceMapper();

            var resultEntity = absenceMapper.MapEntity(viewModel);
            
            Assert.IsNotNull(resultEntity, "the entity returned from mapentity should not be null");
            Assert.IsInstanceOf<Absence>(resultEntity, "the returned entity should an instance of type absence");
        }

        [Test]
        public void Map_WithAbsenceViewModel_ReturnsMappedAbsenceEntity()
        {
            var viewModel = DummyAbsenceViewModel();
            var absenceMapper = new AbsenceMapper();

            var entity = absenceMapper.MapEntity(viewModel);

            Assert.AreEqual(viewModel.StartDateTime, entity.StartDateTime);
            Assert.AreEqual(viewModel.EndDateTime, entity.EndDateTime);
            Assert.AreEqual(viewModel.AbsenceType, entity.AbsenceType);
            Assert.AreEqual(viewModel.Details, entity.Details);
        }

        private AbsenceViewModel DummyAbsenceViewModel()
        {
            var viewModel = new AbsenceViewModel()
            {
                StartDateTime = DateTime.Now.AddDays(-4),
                EndDateTime = DateTime.Now.AddDays(-1),
                Details = "some reason",
                AbsenceType = "Illness"
            };
            return viewModel;
        }
    }
}
