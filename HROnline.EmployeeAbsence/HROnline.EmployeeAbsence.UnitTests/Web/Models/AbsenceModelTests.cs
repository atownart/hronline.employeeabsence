﻿using System;
using System.Linq;
using HROnline.EmployeeAbsence.Entities;
using HROnline.EmployeeAbsence.Service;
using HROnline.EmployeeAbsence.Web.Mappers;
using HROnline.EmployeeAbsence.Web.Models;
using HROnline.EmployeeAbsence.Web.ViewModels;
using Moq;
using NUnit.Framework;

namespace HROnline.EmployeeAbsence.UnitTests.Web.Models
{
    [TestFixture]
    public class AbsenceModelTests
    {
        [Test]
        public void AddAbsence_WithAbsenceViewModel_AddsAbsenceWithEntity()
        {
            var absenceViewModel = new AbsenceViewModel();
            var mockAbsenceService = new Mock<IAbsenceService>();
            var mockAbsenceMapper = new Mock<IAbsenceMapper>();
            mockAbsenceMapper.Setup(mapper => mapper.MapEntity(absenceViewModel)).Returns(new Absence());
            var absenceModel = new AbsenceModel(mockAbsenceService.Object, mockAbsenceMapper.Object);

            absenceModel.AddAbsence(absenceViewModel);

            mockAbsenceService.Verify(absenceService => absenceService.Add(It.IsNotNull<Absence>()), "it should call add with an absence entity");
        }

        [Test]
        public void AddAbsence_WithAbsenceViewModel_AddsAbsenceWithMappedEntity()
        {
            var absenceViewModel = new AbsenceViewModel();
            var absenceEntity = new Absence();
            var mockAbsenceService = new Mock<IAbsenceService>();
            var mockAbsenceMapper = new Mock<IAbsenceMapper>();
            mockAbsenceMapper.Setup(mapper => mapper.MapEntity(absenceViewModel)).Returns(absenceEntity);
            var absenceModel = new AbsenceModel(mockAbsenceService.Object, mockAbsenceMapper.Object);

            absenceModel.AddAbsence(absenceViewModel);

            mockAbsenceService.Verify(absenceService => absenceService.Add(absenceEntity), "it should call add with a mapped absence entity");
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void AddAbsence_WithNullAbsence_NullAbsenceNotAllowed()
        {
            var absenceModel = new AbsenceModel(new Mock<IAbsenceService>().Object, new Mock<IAbsenceMapper>().Object);

            absenceModel.AddAbsence(null);
        }

        [TestCase(4)]
        [TestCase(6)]
        [TestCase(1)]
        public void GetAll_ReturnsAbsences(int amount)
        {
            var mockAbsenceMapper = new Mock<IAbsenceMapper>();
            var mockAbsenceService = new Mock<IAbsenceService>();
            var dummyAbsences = AbsenceTestHelper.DummyAbsences(amount);
            var dummyAbsencesViewModels = AbsenceTestHelper.DummyAbsencesViewModel(amount);
            mockAbsenceService.Setup(serv => serv.GetAll()).Returns(dummyAbsences);
            mockAbsenceMapper.Setup(map => map.MapMany(dummyAbsences)).Returns(dummyAbsencesViewModels);
            var absenceModel = new AbsenceModel(mockAbsenceService.Object, mockAbsenceMapper.Object);

            var result = absenceModel.GetAll();

            Assert.AreEqual(amount,result.Count());
        }
    }
}
