﻿using System;
using HROnline.EmployeeAbsence.Entities;
using HROnline.EmployeeAbsence.Providers;
using HROnline.EmployeeAbsence.Service;
using Moq;
using NUnit.Framework;

namespace HROnline.EmployeeAbsence.UnitTests.Service
{
    [TestFixture]
    public class AbsenceServiceTests
    {
        [Test]
        public void AddAbsence_WithAbsenceEntity_AddsAbsence()
        {
            var mockAbsenceRepository = new Mock<IAbsenceRepostory>();
            var absenceService = new AbsenceService(mockAbsenceRepository.Object);
            var dummyAbsence = new Absence();

            absenceService.Add(dummyAbsence);

            mockAbsenceRepository.Verify(repo => repo.Add(dummyAbsence),Times.Once,"addabsence have added to the absence repository");
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddAbsence_WithNullAbsence_NullAbsenceNotAllowed()
        {
            var absenceService = new AbsenceService(new Mock<IAbsenceRepostory>().Object);

            absenceService.Add(null);
        }
    }
}
