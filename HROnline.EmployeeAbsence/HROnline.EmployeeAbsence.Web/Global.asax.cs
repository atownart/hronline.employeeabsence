﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using HROnline.EmployeeAbsence.Web.App_Start;

namespace HROnline.EmployeeAbsence.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
