﻿using System.Collections;
using System.Collections.Generic;
using HROnline.EmployeeAbsence.Entities;
using HROnline.EmployeeAbsence.Web.ViewModels;

namespace HROnline.EmployeeAbsence.Web.Models
{
    public interface IAbsenceModel
    {
        void AddAbsence(AbsenceViewModel absenceViewModel);

        IEnumerable<AbsenceViewModel> GetAll();
    }
}
