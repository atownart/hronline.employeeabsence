﻿using System;
using System.Collections.Generic;
using HROnline.EmployeeAbsence.Entities;
using HROnline.EmployeeAbsence.Service;
using HROnline.EmployeeAbsence.Web.Mappers;
using HROnline.EmployeeAbsence.Web.ViewModels;

namespace HROnline.EmployeeAbsence.Web.Models
{
    public class AbsenceModel : IAbsenceModel
    {
        private readonly IAbsenceService _absenceService;
        private readonly IAbsenceMapper _absenceMapper;

        public AbsenceModel(IAbsenceService absenceService, IAbsenceMapper absenceMapper)
        {
            _absenceService = absenceService;
            _absenceMapper = absenceMapper;
        }

        public void AddAbsence(AbsenceViewModel absenceViewModel)
        {
            if (absenceViewModel == null)
            {
                throw new ArgumentNullException();
            }

            _absenceService.Add(_absenceMapper.MapEntity(absenceViewModel));
        }

        public IEnumerable<AbsenceViewModel> GetAll()
        {
            var absences = _absenceService.GetAll();
            return _absenceMapper.MapMany(absences);
        }
    }
}