﻿using System.Web.Mvc;
using HROnline.EmployeeAbsence.Entities;
using HROnline.EmployeeAbsence.Web.Models;
using HROnline.EmployeeAbsence.Web.ViewModels;

namespace HROnline.EmployeeAbsence.Web.Controllers
{
    public class AbsenceController:Controller
    {
        private readonly IAbsenceModel _absenceModel;
        public AbsenceController(IAbsenceModel absenceModel)
        {
            _absenceModel = absenceModel;
        }

        public ActionResult Index()
        {
            var absences = _absenceModel.GetAll();
            return View(absences);
        }

        public ActionResult Add()
        {
            var absenceViewModel = new AbsenceViewModel();
            return View(absenceViewModel);
        }

        [HttpPost]
        public ActionResult Add(AbsenceViewModel absenceViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            absenceViewModel.EmployeeId = 1;
            _absenceModel.AddAbsence(absenceViewModel);
            return View("Submitted");
        }
    }
}