﻿using System.Web.Mvc;

namespace BrightHR.EmployeeAttendance.Web.Controllers
{
    public class HomeController :Controller
    {
        public ViewResult Index()
        {
            return View();
        }
    }
}