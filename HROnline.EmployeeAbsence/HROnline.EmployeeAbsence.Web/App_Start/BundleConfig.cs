﻿using System.Web.Optimization;

namespace HROnline.EmployeeAbsence.Web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/script/libs").Include(
                "~/libs/jquery/*.js",
                "~/libs/bootstrap/js/bootstrap.min.js"
                ));

            bundles.Add(new StyleBundle("~/bundles/styles/lib").Include(
                "~/libs/bootstrap/css/bootstrap.css"));
        }
    }
}