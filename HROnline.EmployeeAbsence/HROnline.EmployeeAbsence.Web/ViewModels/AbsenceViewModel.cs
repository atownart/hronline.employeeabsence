﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using HROnline.EmployeeAbsence.Entities;
using HROnline.EmployeeAbsence.Entities.Enums;

namespace HROnline.EmployeeAbsence.Web.ViewModels
{
    public class AbsenceViewModel:IValidatableObject
    {
        [Required]
        public DateTime StartDateTime { get; set; }

        public string FriendlyStartDate
        {
            get { return StartDateTime.ToShortDateString(); }
        }

        [Required]
        public DateTime EndDateTime { get; set; }

        public string FriendlyEndDate
        {
            get { return EndDateTime.ToShortDateString(); }
        }

        public string Details { get; set; }
        [Required]
        public string AbsenceType { get; set; }
        public double Duration { get; set; }
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public AbsenceStatus Status { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (StartDateTime >= EndDateTime)
            {
                yield return new ValidationResult("Start Date must be before End Date", new[] { "StartDateTime" });
            }
            
        }
    }
}