﻿using HROnline.EmployeeAbsence.Entities;
using HROnline.EmployeeAbsence.Web.ViewModels;

namespace HROnline.EmployeeAbsence.Web.Mappers
{
    public interface IAbsenceMapper:IGenericMapper<AbsenceViewModel,Absence>
    {
        
    }
}
