﻿using System;
using System.Collections.Generic;
using AutoMapper;
using HROnline.EmployeeAbsence.Entities;
using HROnline.EmployeeAbsence.Web.ViewModels;

namespace HROnline.EmployeeAbsence.Web.Mappers
{
    public class AbsenceMapper : IAbsenceMapper
    {
        public AbsenceMapper()
        {
            Mapper.CreateMap<AbsenceViewModel, Absence>();
            Mapper.CreateMap<Absence, AbsenceViewModel>();
        }
        public Absence MapEntity(AbsenceViewModel viewModel)
        {
            return Mapper.Map(viewModel, new Absence());
        }

        public IEnumerable<AbsenceViewModel> MapMany(IEnumerable<Absence> entities)
        {
            return Mapper.Map(entities, new List<AbsenceViewModel>());
        }
    }
}