﻿using System.Collections;
using System.Collections.Generic;

namespace HROnline.EmployeeAbsence.Web.Mappers
{
    public interface IGenericMapper<TViewModel,TEntity>
    {
        TEntity MapEntity(TViewModel viewModel);

        IEnumerable<TViewModel> MapMany(IEnumerable<TEntity> entities);
    }
}
