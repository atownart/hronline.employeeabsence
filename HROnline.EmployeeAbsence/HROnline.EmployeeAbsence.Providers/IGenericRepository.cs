﻿using System.Collections;
using System.Collections.Generic;

namespace HROnline.EmployeeAbsence.Providers
{
    public interface IGenericRepository<TEntity>
    {
        void Add(TEntity entity);

        IEnumerable<TEntity> GetAll();
    }
}
