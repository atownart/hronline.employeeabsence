﻿using HROnline.EmployeeAbsence.Entities;

namespace HROnline.EmployeeAbsence.Providers
{
    public interface IAbsenceRepostory:IGenericRepository<Absence>
    {
    }
}
