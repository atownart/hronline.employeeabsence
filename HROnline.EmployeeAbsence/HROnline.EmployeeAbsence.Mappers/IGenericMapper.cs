﻿namespace HROnline.EmployeeAbsence.Mappers
{
    public interface IGenericMapper<TViewModel,TEntity>
    {
        TEntity MapEntity(TViewModel viewModel);
    }
}
