﻿using System.Data.Entity;
using HROnline.EmployeeAbsence.Entities;

namespace HROnline.EmployeeAbsence.Providers.SQL
{
    internal class EmployeeAbsenceContext : DbContext
    {
        internal EmployeeAbsenceContext() : base("AbsenceConnection")
        {
            //turn off auto db creation
            Database.SetInitializer<EmployeeAbsenceContext>(null);
        }

        public DbSet<Absence> Absences { get; set; }

        public DbSet<Employee> Employees { get; set; }
    }
}
