﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using HROnline.EmployeeAbsence.Entities;
using HROnline.EmployeeAbsence.Entities.Enums;

namespace HROnline.EmployeeAbsence.Providers.SQL
{
    public class AbsenceRepository: IAbsenceRepostory
    {
        public void Add(Absence entity)
        {
            using (var dbContext = new EmployeeAbsenceContext())
            {
                entity.Status = AbsenceStatus.WaitingApproval;
                dbContext.Absences.Add(entity);              
                dbContext.SaveChanges();
            }
        }

        public IEnumerable<Absence> GetAll()
        {
            using (var dbContext = new EmployeeAbsenceContext())
            {
                return dbContext.Absences.OrderBy(abs => abs.StartDateTime).Include(ab => ab.Employee).ToList();
            }
        }
    }
}
