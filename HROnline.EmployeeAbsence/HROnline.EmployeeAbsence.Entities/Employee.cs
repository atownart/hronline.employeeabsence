﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace HROnline.EmployeeAbsence.Entities
{
    public class Employee
    {
        public int EmployeeId { get; set; }

        public string ForeName { get; set; }

        public string Surname { get; set; }

        public string FullName => $"{ForeName} {Surname}";

        public ICollection<Absence> Absences { get; set; }
    }
}
