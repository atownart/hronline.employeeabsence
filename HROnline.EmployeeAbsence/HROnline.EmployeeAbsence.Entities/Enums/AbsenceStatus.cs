﻿namespace HROnline.EmployeeAbsence.Entities.Enums
{
    public enum AbsenceStatus
    {
        WaitingApproval,
        Accepted,
        Declined
    }
}
