﻿using System;
using HROnline.EmployeeAbsence.Entities.Enums;

namespace HROnline.EmployeeAbsence.Entities
{
    public class Absence
    {
        public int AbsenceId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string Details { get; set; }
        public string AbsenceType { get; set; }
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public AbsenceStatus Status { get; set; }

        public double Duration
        {
            get
            {
                TimeSpan span = EndDateTime - StartDateTime;
                return span.TotalDays;
            }
        }
    }
}
